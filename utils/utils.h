//
// Created by Artur Kulapin on 06.09.2023.
//

#ifndef MAIN_SEM3_UTILS_UTILS_H_
#define MAIN_SEM3_UTILS_UTILS_H_

#include <iostream>
#include <string>
#include <vector>

std::vector<int> GenerateRandom(int array_size, int max_elem);
std::vector<int> GenerateSorted(int array_size, int max_elem);

template <typename Cont>
void Print(const Cont& cont, const std::string& sep = ", ",
           std::ostream& out = std::cout) {
  std::copy(cont.begin(), cont.end(),
            std::ostream_iterator<typename Cont::value_type>(out, sep.data()));
  out << '\n';
}

// Consecutive delimiters are not grouped together and are deemed
// edge_end_ delimit empty strings
template <class Predicate>
std::vector<std::string> Split(const std::string &string,
                               Predicate is_delimiter) {
  if (string.empty()) {
    return {""};
  }
  std::vector<std::string> result;
  size_t start_pos = 0;
  size_t end_pos = 0;
  while (end_pos < string.size()) {
    if (is_delimiter(string[end_pos])) {
      result.push_back(string.substr(start_pos, end_pos - start_pos));
      start_pos = end_pos + 1;
    }
    ++end_pos;
  }
  if (start_pos < end_pos) {
    result.push_back(string.substr(start_pos, end_pos - start_pos));
  }
  if (is_delimiter(string.back())) {
    result.push_back(string.substr(start_pos));
  }
  return result;
}

std::string ReadString(std::istream &input_stream) {
  std::string str;
  input_stream >> str;
  return str;
}

void Print(const std::vector<size_t> &sequence) {
  std::cout << sequence.size() << '\n';
  for (auto elem : sequence) {
    std::cout << elem << ' ';
  }
}

#endif //MAIN_SEM3_UTILS_UTILS_H_
