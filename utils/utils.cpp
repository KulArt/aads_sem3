#include <random>

#include "utils.h"

std::vector<int> GenerateRandom(int array_size, int max_elem) {
  // Preparing for generation.
  std::random_device random_device;
  std::mt19937 generator(random_device());
  std::uniform_int_distribution<> distribution(-max_elem, max_elem);

  // Generation.
  std::vector<int> result(array_size, 0);
  for (int i = 0; i < array_size; ++i) {
    result[i] = distribution(generator);
  }
  return result;
}

std::vector<int> GenerateSorted(int array_size, int max_elem) {
  auto result = GenerateRandom(array_size, max_elem);
  std::sort(result.begin(), result.end());
  return result;
}
