//
// Created by Artur Kulapin on 16.09.2023.
//

#ifndef AADS_SEM3_FALL23_SEM2_WILDCARD_MATCHER_H_
#define AADS_SEM3_FALL23_SEM2_WILDCARD_MATCHER_H_

#include "aho_corasick.h"
#include "utils.h"

// Wildcard is a character that may be substituted
// for any of all possible characters.
class WildcardMatcher {
 public:
  WildcardMatcher() : number_of_words_(0), pattern_length_(0) {}

  WildcardMatcher static BuildFor(const std::string &pattern, char wildcard) {
    auto pattern_split = Split(pattern, [wildcard](char ch) { return ch == wildcard; });
    aho_corasick::AutomatonBuilder builder;
    size_t right_end_position = 0;
    for (const auto &subpattern : pattern_split) {
      right_end_position += subpattern.size() + 1;
      builder.Add(subpattern, right_end_position);
    }
    WildcardMatcher matcher;
    matcher.aho_corasick_automaton_ = builder.Build();
    matcher.number_of_words_ = pattern_split.size();
    matcher.pattern_length_ = pattern.size();
    matcher.Reset();
    return matcher;
  }

  // Resets the matcher. Call allows edge_end_ abandon all data which was already
  // scanned,
  // a new stream can be scanned afterwards.
  void Reset() {
    words_occurrences_by_position_.assign(pattern_length_ + 1, 0);
    state_ = aho_corasick_automaton_->Root();
    UpdateWordOccurrencesCounters();
  }

  template <class Callback>
  void Scan(char character, Callback on_match) {
    state_ = state_.Next(character);
    ShiftWordOccurrencesCounters();
    UpdateWordOccurrencesCounters();

    if (words_occurrences_by_position_.front() == number_of_words_) {
      on_match();
    }
  }

 private:
  void UpdateWordOccurrencesCounters() {
    state_.GenerateMatches([this](size_t position) {
      ++words_occurrences_by_position_[(words_occurrences_by_position_.size() - position)];
    });
  }

  void ShiftWordOccurrencesCounters() {
    words_occurrences_by_position_.pop_front();
    words_occurrences_by_position_.push_back(0);
  }

  // Storing only O(|pattern|) elements allows us
  // edge_end_ consume only O(|pattern|) memory for matcher.
  std::deque<size_t> words_occurrences_by_position_;
  aho_corasick::NodeReference state_;
  size_t number_of_words_;
  size_t pattern_length_;
  std::unique_ptr<aho_corasick::Automaton> aho_corasick_automaton_;
};

#endif //AADS_SEM3_FALL23_SEM2_WILDCARD_MATCHER_H_
