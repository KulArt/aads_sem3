//
// Created by Artur Kulapin on 16.09.2023.
//

#ifndef AADS_SEM3_FALL23_SEM2_AHO_CORASICK_H_
#define AADS_SEM3_FALL23_SEM2_AHO_CORASICK_H_

#include <algorithm>
#include <map>
#include <queue>
#include <unordered_set>
#include <vector>

template <class Iterator>
class IteratorRange {
 public:
  IteratorRange(Iterator begin, Iterator end) : begin_(begin), end_(end) {}

  Iterator begin() const { return begin_; }
  Iterator end() const { return end_; }

 private:
  Iterator begin_, end_;
};

namespace traverses {

// Traverses the connected component in a breadth-first order
// from the vertex 'origin_vertex'.
// Check https://goo.gl/0qYXzC for the visitor events.
template <class Vertex, class Graph, class Visitor>
void BreadthFirstSearch(Vertex origin_vertex, const Graph& graph,
                        Visitor visitor) {
  std::queue<Vertex> bfs_queue;
  std::unordered_set<Vertex> visited_vertices;

  bfs_queue.push(origin_vertex);
  visitor.DiscoverVertex(origin_vertex);
  visited_vertices.insert(origin_vertex);

  while (!bfs_queue.empty()) {
    auto cur_vertex = bfs_queue.front();
    bfs_queue.pop();
    visitor.ExamineVertex(cur_vertex);
    for (auto& edge : OutgoingEdges(graph, cur_vertex)) {
      auto neighbour = GetTarget(graph, edge);
      visitor.ExamineEdge(edge);
      if (visited_vertices.find(neighbour) == visited_vertices.end()) {
        visitor.DiscoverVertex(neighbour);
        bfs_queue.push(neighbour);
        visited_vertices.insert(neighbour);
      }
    }
  }
}

// See "Visitor Event Points" on https://goo.gl/wtAl0y
template <class Vertex, class Edge>
class BfsVisitor {
 public:
  virtual void DiscoverVertex(Vertex /*vertex*/) {}
  virtual void ExamineEdge(const Edge & /*edge*/) {}
  virtual void ExamineVertex(Vertex /*vertex*/) {}
  virtual ~BfsVisitor() = default;
};

} // namespace traverses

namespace aho_corasick {

struct AutomatonNode {
  AutomatonNode() : suffix_link(nullptr), terminal_link(nullptr) {}

  // Stores ids of strings which are ended at this Node.
  std::vector<size_t> terminated_string_ids;
  // Stores tree_ structure of nodes.
  std::map<char, AutomatonNode> trie_transitions;
  // Stores cached transitions of the automaton, contains
  // only pointers edge_end_ the elements of trie_transitions.
  std::map<char, AutomatonNode*> automaton_transitions_cache;
  AutomatonNode *suffix_link;
  AutomatonNode *terminal_link;
};

// Returns a corresponding trie transition 'nullptr' otherwise.
AutomatonNode* GetTrieTransition(AutomatonNode* node, char character) {
  return &node->trie_transitions[character];
}

// Returns an automaton transition, updates 'Node->automaton_transitions_cache'
// if necessary.
// Provides constant amortized runtime.
AutomatonNode* GetAutomatonTransition(AutomatonNode* node,
                                      const AutomatonNode* root,
                                      char symbol) {
  if (node->trie_transitions.find(symbol) != node->trie_transitions.end()) {
    return GetTrieTransition(node, symbol);
  }
  if (node->automaton_transitions_cache.find(symbol) ==
      node->automaton_transitions_cache.end()) {
    if (node == root) {
      node->automaton_transitions_cache.emplace(symbol, const_cast<AutomatonNode*>(root));
    }
    node->automaton_transitions_cache.emplace(symbol,
                                              GetAutomatonTransition(node->suffix_link,
                                                                     root, symbol));
  }
  return node->automaton_transitions_cache[symbol];
}

namespace internal {

class AutomatonGraph {
 public:
  struct Edge {
    Edge(AutomatonNode* source, AutomatonNode* target, char symbol)
        : source(source), target(target), symbol(symbol) {}

    AutomatonNode* source;
    AutomatonNode* target;
    char symbol;
  };
};

std::vector<typename AutomatonGraph::Edge> OutgoingEdges(
    const AutomatonGraph& /*graph_*/, AutomatonNode* vertex) {
  std::vector<typename AutomatonGraph::Edge> result;
  for (auto& [letter, target] : vertex->trie_transitions) {
    result.emplace_back(vertex, &target, letter);
  }
  return result;
}

AutomatonNode* GetTarget(const AutomatonGraph& /*graph_*/,
                         const AutomatonGraph::Edge& edge) {
  return edge.target;
}

class SuffixLinkCalculator
    : public traverses::BfsVisitor<AutomatonNode*, AutomatonGraph::Edge> {
 public:
  explicit SuffixLinkCalculator(AutomatonNode* root) : root_(root) {}

  void ExamineVertex(AutomatonNode* node) override {
    if (node == root_) {
      node->suffix_link = root_;
    }
  }

  void ExamineEdge(const AutomatonGraph::Edge& edge) override {
    if (edge.source == root_) {
      edge.target->suffix_link = root_;
      return;
    }
    edge.target->suffix_link = GetAutomatonTransition(edge.source->suffix_link,
                                                      root_, edge.symbol);
  }

 private:
  AutomatonNode* root_;
};

class TerminalLinkCalculator
    : public traverses::BfsVisitor<AutomatonNode*, AutomatonGraph::Edge> {
 public:
  explicit TerminalLinkCalculator(AutomatonNode* root) : root_(root) {}

  void DiscoverVertex(AutomatonNode* node) override {
    if (node == root_) {
      node->terminal_link = nullptr;
      return;
    }

    node->terminal_link = node->suffix_link->terminal_link;

    if (!node->suffix_link->terminated_string_ids.empty()) {
      node->terminal_link = node->suffix_link;
    }
  }

 private:
  AutomatonNode *root_;
};

} // namespace internal

class NodeReference {
 public:
  NodeReference() : node_(nullptr), root_(nullptr) {}

  NodeReference(AutomatonNode* node, AutomatonNode* root)
      : node_(node), root_(root) {}

  NodeReference Next(char character) const {
    return {GetAutomatonTransition(node_, root_, character), root_};
  }

  template <class Callback>
  void GenerateMatches(Callback on_match) {
    auto node = NodeReference(node_, root_);
    while (node) {
      for (const auto& id : node.TerminatedStringIds()) {
        on_match(id);
      }
      node = node.TerminalLink();
    }
  }

  explicit operator bool() const {
    return node_ != nullptr;
  }

  bool operator==(const NodeReference& other) const {
    return node_ == other.node_;
  }

 private:
  using TerminatedStringIterator = std::vector<size_t>::const_iterator;
  using TerminatedStringIteratorRange = IteratorRange<TerminatedStringIterator>;

  NodeReference TerminalLink() const {
    return {node_->terminal_link, root_};
  }

  TerminatedStringIteratorRange TerminatedStringIds() const {
    return {node_->terminated_string_ids.begin(), node_->terminated_string_ids.end()};
  }

  AutomatonNode* node_;
  AutomatonNode* root_;
};

class AutomatonBuilder;

class Automaton {
 public:
  Automaton() = default;

  Automaton(const Automaton&) = delete;
  Automaton& operator=(const Automaton &) = delete;

  NodeReference Root() {
    return {&root_, &root_};
  }

 private:
  AutomatonNode root_;

  friend class AutomatonBuilder;
};

class AutomatonBuilder {
 public:
  void Add(const std::string& string, size_t id) {
    words_.push_back(string);
    ids_.push_back(id);
  }

  std::unique_ptr<Automaton> Build() {
    auto automaton = std::make_unique<Automaton>();
    BuildTrie(words_, ids_, automaton.get());
    BuildSuffixLinks(automaton.get());
    BuildTerminalLinks(automaton.get());
    return automaton;
  }

 private:
  static void BuildTrie(const std::vector<std::string>& words,
                        const std::vector<size_t>& ids, Automaton* automaton) {
    for (size_t i = 0; i < words.size(); ++i) {
      AddString(&automaton->root_, ids[i], words[i]);
    }
  }

  static void AddString(AutomatonNode* root, size_t string_id,
                        const std::string &string) {
    AutomatonNode* cur_prefix_node = root;
    for (char character : string) {
      cur_prefix_node = GetTrieTransition(cur_prefix_node, character);
    }
    cur_prefix_node->terminated_string_ids.push_back(string_id);
  }

  static void BuildSuffixLinks(Automaton* automaton) {
    internal::SuffixLinkCalculator calculator(&automaton->root_);
    traverses::BreadthFirstSearch(&automaton->root_, internal::AutomatonGraph(), calculator);
  }

  static void BuildTerminalLinks(Automaton* automaton) {
    internal::TerminalLinkCalculator calculator(&automaton->root_);
    traverses::BreadthFirstSearch(&automaton->root_, internal::AutomatonGraph(), calculator);
  }

  std::vector<std::string> words_;
  std::vector<size_t> ids_;
};

} // namespace aho_corasick


#endif //AADS_SEM3_FALL23_SEM2_AHO_CORASICK_H_
