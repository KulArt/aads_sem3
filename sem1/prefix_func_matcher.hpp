//
// Created by Артур Кулапин on 31.01.2023.
//

#ifndef TEACHING_SEMESTER_3_SEM1_PREFIX_FUNC_MATCHER_H_
#define TEACHING_SEMESTER_3_SEM1_PREFIX_FUNC_MATCHER_H_

#include "abstract_matcher.hpp"

#include <numeric>
#include <vector>

template <class Container>
std::vector<uint64_t> CalculateCutPrefixFunction(const Container& container,
                                                 const uint64_t limit = std::numeric_limits<uint64_t>::max()) {
  std::vector<uint64_t> result(container.size(), 0);
  auto right_iter = ++container.begin();

  for (size_t i = 1; i < container.size(); ++i) {
    uint64_t cur_pi_value = result[i - 1];
    auto pi_iter = container.begin();
    std::advance(pi_iter, cur_pi_value);

    while (cur_pi_value > 0 && *right_iter != *pi_iter) {
      cur_pi_value = result[cur_pi_value - 1];
      pi_iter = container.begin();
      std::advance(pi_iter, cur_pi_value);
    }

    if (*right_iter == *pi_iter) {
      ++cur_pi_value;
    }
    result[i] = std::min(cur_pi_value, limit);
    ++right_iter;
  }

  return result;
}

template <class Container>
Container Concatenate(const Container& lhs, const Container& rhs) {
  Container result = lhs;
  for (const auto& elem : rhs) {
    result.push_back(elem);
  }
  return result;
}


template <class Container, class Callback>
class PrefixFuncMatcher : public AbstractMatcher<Container, Callback> {
 public:
  using BaseClass = AbstractMatcher<Container, Callback>;
  explicit PrefixFuncMatcher(Container container, Callback callback) : BaseClass(container, callback) {
  }

  void Match(const Container& pattern) const final {
    // Requires some guard_element. But let's just cut prefix function if it's
    // value bigger than pattern.size().
    auto pi_function = CalculateCutPrefixFunction(Concatenate(pattern,BaseClass::text_),pattern.size());

    for (size_t i = pattern.size(); i < pi_function.size(); ++i) {
      if (pi_function[i] == pattern.size()) {
        BaseClass::callback_(i);
      }
    }
  }
};


#endif //TEACHING_SEMESTER_3_SEM1_PREFIX_FUNC_MATCHER_H_
