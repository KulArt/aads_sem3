//
// Created by Артур Кулапин on 31.01.2023.
//

#ifndef TEACHING_SEMESTER_3_SEM1_PREFIX_FUNC_MATCHER_HPP_
#define TEACHING_SEMESTER_3_SEM1_PREFIX_FUNC_MATCHER_HPP_

#include "abstract_matcher.hpp"
#include "sequence_hasher.hpp"

template <class Container, class Callback>
class RabinKarpMatcher : public AbstractMatcher<Container, Callback> {
 public:
  using BaseClass = AbstractMatcher<Container, Callback>;
  explicit RabinKarpMatcher(Container container, Callback callback) : BaseClass(container, callback) {
    text_hasher_ = SequenceHasher<Container>(&(BaseClass::text_));
  }

  void Match(const Container& pattern) const final {
    const size_t pattern_size = pattern.size();
    auto pattern_hasher = SequenceHasher<Container>(&pattern);

    auto iter_end = BaseClass::text_.begin();
    std::advance(iter_end, BaseClass::text_.size() - pattern_size + 1);

    auto right_iter = BaseClass::text_.begin();
    std::advance(right_iter, pattern_size);

    // Can you find any weak places in that code? May be extra time consumption.
    for (auto left_iter = BaseClass::text_.begin(); left_iter < iter_end; ++left_iter) {
      if (pattern_hasher.CalculateSubsegmentHash(pattern.cbegin(), pattern.cend()) ==
          text_hasher_.CalculateSubsegmentHash(left_iter, right_iter)) {
        BaseClass::callback_(left_iter - BaseClass::text_.begin());
      }
      ++right_iter;
    }
  }

 private:
  SequenceHasher<Container> text_hasher_;
};


#endif //TEACHING_SEMESTER_3_SEM1_PREFIX_FUNC_MATCHER_HPP_

