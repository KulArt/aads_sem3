//
// Created by Артур Кулапин on 31.01.2023.
//

#include <iostream>

#include "prefix_func_matcher.hpp"
#include "rabin_karp_matcher.hpp"

int main() {
  const std::vector<int> text = {1, 2, 1, 3, 1, 2, 1};
  const std::vector<int> pattern{1, 2, 1};

  auto print = [shift = 2 * pattern.size() - 1](size_t i) { std::cout << i - shift << '\n'; };
  PrefixFuncMatcher<std::vector<int>, decltype(print)> p_matcher(text, print);
  p_matcher.Match(pattern);

  auto iter_print = [](size_t i) { std::cout << i << '\n'; };
  RabinKarpMatcher<std::vector<int>, decltype(iter_print)> h_matcher(text, iter_print);
  h_matcher.Match(pattern);
}
